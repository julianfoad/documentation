import { defineConfig } from 'vitepress'

export default defineConfig({
  lang: 'en-US',
  title: 'PeerTube documentation',
  description: 'Documentation of PeerTube, a free software to take back control of your videos!',

  outDir: '../public',

  cleanUrls: true,

  rewrites: {
    'remote-markdown/CHANGELOG.md': 'CHANGELOG.md',
    'remote-markdown/CODE_OF_CONDUCT.md': 'CODE_OF_CONDUCT.md',
    'remote-markdown/CONTRIBUTING.md': 'CONTRIBUTING.md',
    'remote-markdown/CREDITS.md': 'CREDITS.md',
    'remote-markdown/doc/:file': 'support/doc/:file',
    'remote-markdown/doc/api/:file': 'support/doc/api/:file',
    'remote-markdown/doc/development/:file': 'support/doc/development/:file',
    'remote-markdown/doc/plugins/:file': 'support/doc/plugins/:file'
  },

  markdown: {
    toc: {
      level: [ 2, 3, 4 ]
    }
  },

  themeConfig: {
    logo: {
      light: '/brand-light.png',
      dark: '/brand-dark.png'
    },

    siteTitle: false,

    sidebar: {
      '/': englishSidebar(),
      '/fr/': frenchSidebar()
    },

    editLink: {
      pattern: 'https://framagit.org/framasoft/peertube/documentation/-/edit/master/docs/:path',
      text: 'Edit this page'
    },

    socialLinks: [
      { icon: 'github', link: 'https://github.com/Chocobozzz/PeerTube' }
    ],

    i18nRouting: false,

    outline: {
      level: 'deep'
    },

    search: {
      provider: 'local'
    }
  },

  locales: {
    root: {
      label: 'English',
      lang: 'en'
    },

    fr: {
      label: 'Français',
      lang: 'fr'
    }
  }
})

function englishSidebar() {
  return [

    {
      text: 'Install or upgrade PeerTube',
      items: [
        { text: 'Any OS (recommended)', link: '/install/any-os' },
        { text: 'Docker', link: '/install/docker' },
        { text: 'Unofficial', link: '/install/unofficial' }
      ]
    },

    {
      text: 'Maintain PeerTube (sysadmin)',
      items: [
        { text: 'CLI tools', link: '/maintain/tools' },
        { text: 'Remote storage (S3)', link: '/maintain/remote-storage' },
        { text: 'System configuration', link: '/maintain/configuration' },
        { text: 'Instance migration', link: '/maintain/migration' },
        { text: 'Monitoring/Observability', link: '/maintain/observability' }
      ]
    },

    {
      text: 'Administer PeerTube',
      items: [
        { text: 'Instance follows & redundancy', link: '/admin/following-instances' },
        { text: 'Manage Users & Auth', link: '/admin/managing-users' },
        { text: 'Moderate your instance', link: '/admin/moderation' },
        { text: 'Configuration', link: '/admin/configuration' },
        { text: 'Remote runners', link: '/admin/remote-runners' },
        { text: 'Customize your instance', link: '/admin/customize-instance' },
        { text: 'PeerTube logs', link: '/admin/logs' }
      ]
    },

    {
      text: 'Use PeerTube',
      items: [
        { text: 'Watch, share, download a video', link: '/use/watch-video' },
        { text: 'Setup your account', link: '/use/setup-account' },
        { text: 'User library', link: '/use/library' },
        { text: 'Publish a video or a live', link: '/use/create-upload-video' },
        { text: 'Studio: quick edit for your videos', link: '/use/studio' },
        { text: 'Video statistics', link: '/use/video-stats' },
        { text: 'Sync a remote channel', link: '/use/channel-sync' },
        { text: 'Search', link: '/use/search' },
        { text: 'Mute instances/accounts', link: '/use/mute' },
        { text: 'Report content', link: '/use/report' },
        { text: 'Third-party applications', link: '/use/third-party-application' }
      ]
    },

    {
      text: 'Contribute on PeerTube',
      items: [
        { text: 'Getting started', link: '/contribute/getting-started' },
        { text: 'Plugins & Themes', link: '/contribute/plugins' },
        { text: 'Architecture', link: '/contribute/architecture' },
        { text: 'Code of conduct', link: '/contribute/code-of-conduct' }
      ]
    },

    {
      text: 'PeerTube API',
      items: [
        { text: 'Custom client markup', link: '/api/custom-client-markup' },
        { text: 'Getting started with REST API', link: '/api/rest-getting-started' },
        { text: 'REST API reference', link: 'pathname:///api-rest-reference.html' },
        { text: 'Plugins & Themes API', link: '/api/plugins' },
        { text: 'Player embed API', link: '/api/embed-player' },
        { text: 'ActivityPub', link: '/api/activitypub' },
      ]
    }
  ]
}

function frenchSidebar () {
  return [
    {
      text: 'Utiliser PeerTube',
      items: [
        { text: 'Configurer son profil', link: '/fr/use/setup-account' },
        { text: 'Visionner, partager et télécharger une vidéo', link: '/fr/use/share-video' },
        { text: 'Télécharger une vidéo', link: '/fr/use/download-video' },
        { text: 'Chaîne de vidéos', link: '/fr/use/video-channels' },
        { text: 'Playlistes de vidéos', link: '/fr/use/video-playlist' },
        { text: 'Publier une vidéo ou un direct', link: '/fr/use/create-upload-video' },
        { text: 'Modifier ses vidéos depuis l\'interface web', link: '/fr/use/studio' },
        { text: 'Historique des vidéos', link: '/fr/use/video-history' },
        { text: 'Recherche', link: '/fr/use/search' },
        { text: 'Signaler un contenu', link: '/fr/use/report' }
      ]
    }
  ]
}
