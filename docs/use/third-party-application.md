# Third party applications

## Browser addons

 * [PeerTube Companion](https://codeberg.org/Booteille/peertube-companion): browser addon redirecting your YouTube videos to PeerTube if they exist on it. Available on [Firefox](https://addons.mozilla.org/firefox/addon/peertube-companion/)

## Android applications

 * [Thorium](https://github.com/sschueller/peertube-android): an Android PeerTube Client
 * [Fedilab](https://fedilab.app/): Fedilab is a multifunctional Android client to access the distributed Fediverse, consisting of microblogging, photo sharing and video hosting
 * [P2Play](https://gitlab.com/agosto182/p2play): Android PeerTube client
 * [NewPipe](https://github.com/TeamNewPipe/NewPipe/): NewPipe is a libre lightweight streaming front-end for Android. PeerTube is supported
 * [TubeLab](https://framagit.org/tom79/fedilab-tube): TubeLab is a PeerTube client for French academics

## Kodi addons

 * [plugin.video.peertube](https://framagit.org/StCyr/plugin.video.peertube): KODI plugin that allows streaming from instances listed publicly, or custom ones you set. It only downloads via libtorrent that doesn't support WebTorrent

## Roku applications

 * [PeerVue](https://github.com/n76/PeerVue): A PeerTube client for Roku. It can also be added to your Roku account as a private channel by using the code [PEERVUE](https://my.roku.com/add/PEERVUE)

## CLI scripts

 * [peertubetomasto](https://github.com/PhieF/MiscConfig/blob/master/Peertube/peertubetomasto.py): toot new videos on Mastodon automatically (Python)
 * [Prismedia](https://git.lecygnenoir.info/LecygneNoir/prismedia): scripting video uploads to PeerTube and YouTube at the same time (Python)
 * [YouTube2PeerTube](https://github.com/mister-monster/YouTube2PeerTube): a bot that mirrors YouTube channels to PeerTube channels as videos are released (Python)
 * [youtube-dl](https://youtube-dl.org/): Download video, audio, thumbnail in every available formats easily

## Web tools

 * [PeerTubatrix](https://booteille.codeberg.page/peertubatrix/): a tool to generate uMatrix rules for known PeerTube instances
 * [SimpleerTube](https://codeberg.org/SimpleWeb/SimpleerTube): an alternative web client for PeerTube (no JavaScript)
 * [PeerTube Extensions Finder](https://booteille.codeberg.page/peertube-extensions-finder/): a tool to find extensions available for PeerTube. Useful for non-admin users to know which extensions exist easily.

## Desktop clients

 * [Cuttlefish](https://gitlab.shinice.net/artectrex/Cuttlefish): GNOME PeerTube client
 * [peertube-viewer](https://peertube-viewer.com): Command line PeerTube client, similar to youtube-viewer
 * [peertube-cli](https://github.com/dimethyltriptamine/peertube-cli.git): Command line PeerTube client.
